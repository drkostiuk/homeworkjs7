function filterBy(arr, dataType) {
  return arr.filter((item) => typeof item !== dataType);
}

// Приклад використання
const data = ["hello", "world", 23, "23", null];
const filteredData = filterBy(data, "string");
console.log(filteredData); // [23, null]

// 1. Метод forEach в JavaScript використовується для ітерації (перебору) елементів
//  масиву і виконання певної функції з кожним елементом.

// Основна ідея методу forEach полягає в тому, щоб дозволити нам легко ітерувати по всіх елементах масиву без необхідності
//  використовувати цикли for або while.

// 2. Є декілька способів.
// 1) задати змінній, в якій зберігається масив, порожній масив:
// let arr = ['d', 5, null, "userName"];
// arr = [];
// Працює лише зі змінною let, яку можна перезаписати. Крім того, по факту, це складно назвати очищенням масиву,
// адже ми просто присвоюємо змінній значення іншого, порожнього масиву.

// Другий - встановити довжину масиву рівну нулю:
// let arr = ['d', 5, null, "userName"];
// arr.length = 0;

// Третій - сплайс від першого до останнього елементу:
// let arr = ['d', 5, null, "userName"];
// arr.splice(0, arr.length);
// 3. За допомогою Array.isArray(array):
// let arr = ['d', 5, null, "userName"];
// Array.isArray(arr); // true;
// let notArr = 'd, 5, null, userName';
// Array.isArray(notArr); // false;
